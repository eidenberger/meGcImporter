#!/usr/bin/env python
# -*- coding: utf-8 -*-
# a Simple and dirty script in order to import google categories in mongo

import string, os, sys, logging, unicodedata, re


# Create a global logger...
x = logging.getLogger("meLogger")
x.setLevel(logging.ERROR) # INFO DEBUG ERROR
h = logging.StreamHandler()
#f = logging.Formatter("[%(levelname)s] %(asctime)s %(funcName)s %(lineno)d %(message)s")
f = logging.Formatter("[%(levelname)s] %(asctime)s : %(message)s")
h.setFormatter(f)
x.addHandler(h)

### Open the data
#data = open("data").readlines()

class meGcImporter(object):
	"""
	meGcImporter importer class, used to process
	google shopping categories taxonomy. See http://www.kenkai.com/seo-blog-article-346.htm
	"""
	def __init__(self, filename = "taxonomy.fr-FR.txt"):
		
		# Set file name
		self.filename = filename
		
		# Set catalog dict. This will be populated by the script
		self.categories = dict({})
		
		# Set Logger
		self.log = logging.getLogger('meLogger')
		
		# Import mongodb module
		try:
			import pymongo # sudo apt-get install python-dev
			from pymongo import Connection
		except Exception, e:
			self.log.error("Missing module" + str(e))
			sys.exit()
		
		# Create pymongo instance
		self.mg = Connection('localhost', 27017)
		self.log.info("Mongodb connection established")
		
		# Set database
		self.db = self.mg['product_categories']
		
		self.parseFile()
	
	def parseFile(self):
		
		data = open(self.filename).readlines() # open the file

		i = 0
		for line in data:
			i += 1
			if (i > 1) :
				tmp_categories = line.split(" > ")
				self.processCategories(tmp_categories)
				
	def processCategories(self, tmp_categories) :
		
		if (len(tmp_categories) == 1) : 
			
			cat = {'title' : tmp_categories[0].strip(), 'parent' : "0"}
			self.tryStore(cat)		
			
		else :
			i = 0
			for tmp_category in tmp_categories :
				if (i > 0) :
					parent_id = self.getParentId(tmp_categories[i-1])
					cat = {'title' : tmp_category.strip(), 'parent' : parent_id}
					self.tryStore(cat)
					if (i == 8) : 
						sys.exit()					
					
					
				i += 1
				#self.log.info(tmp_category.strip())
	def getParentId(self, category_name) : 
		parent = self.categories[self.formatUrlRewrite(category_name)]
		return str(parent['_id'])
	
	def formatUrlRewrite(self, text):
		
		if type(text) == type(str()) : 
			text = text.decode('utf8', errors = 'ignore')				
					
		text = unicodedata.normalize("NFKD", unicode(text)).encode("ascii", errors = 'ignore')
		return re.compile("\W+", re.UNICODE).sub("_", text)
	
	def tryStore(self, cat) :
		
		# query in order to determine if category is known or not...
		res = self.db.categories_fr.find_one(cat)
		
		if(res == None) : 
			# category hasn't been found... insert
			self.db.categories_fr.insert(cat)
			self.log.info('Successfully inserted category')
		else : 
			# overwrite cat with the one found in collection
			cat = res
			self.log.info('"' + unicode(cat['title']).encode('utf-8') + '" allready in collection')
			
		
		self.categories[self.formatUrlRewrite(cat['title'])] = cat

meGcImporter()